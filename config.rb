# layouts
page "/manual/*", layout: "manual"
page "/examples/*", layout: "examples"
page "/installation/*", layout: "installation"

activate :hashicorp do |h|
  h.version         = "0.0.4"
  h.bintray_enabled = true
  h.bintray_repo    = "promisedlandt/dom4tools"
  h.bintray_user    = "promisedlandt"
  h.bintray_key     = ENV["BINTRAY_API_KEY"]
end

activate :s3_sync do |s|
  s.bucket = "dom4tools.net"
  s.region = "eu-west-1"
end

#activate :cdn do |cdn|
  #cdn.cloudfront = {
    #access_key_id: ENV["AWS_ACCESS"]
  #}
#end

#after_s3_sync do |files_by_status|
  #cdn_invalidate(files_by_status[:updated])
#end

# Build-specific configuration
configure :build do
end
