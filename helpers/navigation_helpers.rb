module NavigationHelpers
  def active_element(name)
    result = ""
    current = current_page.data.drawer_current || ""

    result = " active" if current == name || (name.is_a?(Regexp) && name.match(current))

    result
  end

  def command_path(command)
    name = command.is_a?(Hash) ? command.name : command
    "/manual/commands/#{ name }.html"
  end
end
