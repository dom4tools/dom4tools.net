# dom4tools.net

This is the source for [dom4tools.net](http://dom4tools.net).

It leverages [middleman](https://middlemanapp.com/).

# Getting started

Clone this repository, then install the needed gems with `bundle install --binstubs`.

If [direnv](http://direnv.net/) is installed, allow this .envrc with `direnv allow`.

Start the development server with `foreman start`.

The website will be available at [http://localhost:4567](http://localhost:4567).

# Building

Set the environment variable BINTRAY_API_KEY, then `middleman build`.

# Releasing

Enter AWS credentials into `.s3_sync`, then `middleman s3_sync`.
